#!/usr/bin/env python3

"""
This program renames a number of files. Which match the input of the user.

How to use:
  ./myfile.py "directory to use"

"""

from os import path, listdir, rename

__author__ = 'ezcoding'


def main(argv):
    path_was_not_provided(argv)
    script, *directory_path = argv
    directory_path = " ".join(directory_path)
    check_for_completeness(argv, directory_path)
    word_to_replace = ask_which_word_to_replace()
    files_to_rename = [fn for fn in listdir(directory_path) if word_to_replace in fn]

    if len(files_to_rename) == 0:
        print_no_files_found(directory_path, word_to_replace)

    print_content(files_to_rename)
    word_to_set = ask_which_word_to_set()
    show_how_renamed_files_would_look_like(files_to_rename, word_to_replace, word_to_set)

    if ask_for_permission_to_rename():
        rename_files(directory_path, files_to_rename, word_to_replace, word_to_set)


def path_was_not_provided(argv):
    """
    Checks whether a parameter was passed when the script was called.
    Exits if the len of the argv is < 2
    :param argv: The arguments passed by the callee when this program was opened
    """
    if len(argv) < 2:
        error("""\nPlease provide an existing path to an existing directory as parameter.
This program won't work otherwise.\n""")


def check_for_completeness(argv, directory_path):
    """
    Checks if a path argument was provided. Also checks if the path is an existing directory or not.
    If any of those checks fail, the program will exit with an error message
    :param argv:
    :param directory_path: The path to the directory which we should check. This argument should be provided by the user
    """
    if path_was_provided(argv) is False:
        default_error_message()
        exit()

    if path_exists_and_is_directory(directory_path) is False:
        exit()


def ask_which_word_to_replace():
    """
    Ask the user for an input for a word which should be replaced in the current specified folder
    :return: the word, which will be searched for in the current file names
    """
    prompt_text = "Which part of the file name(s) should be replaced? "
    answer = input(prompt_text)
    while len(answer) == 0:
        answer = input(prompt_text)

    return answer


def ask_which_word_to_set():
    """
    Prompts the user to input the new names of the files
    :return: the input of the user
    """
    print("\n--------")

    return input("How should those files be renamed (only the letters of your input will be replaced): ")


def show_how_renamed_files_would_look_like(files_to_rename, word_to_replace, word_to_set):
    print("\nThe new files will be named like this:\n")

    for file in files_to_rename:
        print(file.replace(word_to_replace, word_to_set))

    print("\n--------")


def ask_for_permission_to_rename():
    prompt_text = "\nAre you sure you want to rename the files? Type y or n: "
    answer = input(prompt_text)

    while answer not in ('y', 'n'):
        answer = input(prompt_text)

    return answer == "y"


def rename_files(directory_path, files_to_rename, word_to_replace, word_to_set):
    """
    Rename files and exists the programm in the end
    :param directory_path: The path to the directory which contain all the files, which will be renamed
    :param files_to_rename: An array of files which will be renamed
    :param word_to_replace: The word which will be replaced in the original file
    :param word_to_set: The letters which will replace the original ones in the original file
    """
    print("\n")
    for file in files_to_rename:
        old_file = directory_path + "/" + file
        new_file = directory_path + "/" + file.replace(word_to_replace, word_to_set)
        print("Renaming: {0}".format(old_file))
        rename(old_file, new_file)

    print("\nDone! Exiting now...")
    exit()


def print_no_files_found(directory_path, word_to_replace):
    """
    Prints that no files where found and exits the program
    :param directory_path: The path of the directory in which the filenames where searched for
    :param word_to_replace: The word which should be used replaced
    """
    print("No files found in \"{0}\" which contain the word \"{1}\"...".format(directory_path, word_to_replace))
    exit()


def path_was_provided(argv):
    """
    Check wether we have more than 1 argument. which we want

    :param argv:
    :return: True, if we have more than the script argument to work with
    """
    return len(argv) > 1


def path_exists_and_is_directory(directory_path):
    """
    Check if the provided directory path is a directory and if it truly exists

    :param directory_path: The path to the directory which will be checked
    :return: True if the provided path exists and if it is a directory
    """
    exists = False
    error_message = "Provided path %s " % directory_path
    if path.exists(directory_path) is False:
        default_error_message(error_message + "doesn't exists")
    elif path.isdir(directory_path) is False:
        default_error_message(error_message + "is not a directory")
    else:
        exists = True

    return exists


def print_content(files_to_rename):
    print("\nThe Following files will be renamed:")
    for file in files_to_rename:
        print(file)


def default_error_message(message=None):
    """
    Prints to default error messages. The first one will always be: I can't work like this...
    The second one may be passed as param.

    :param message: the reason why the program will exit, which will be printed to the user. If None is passed, we print our default message instead
    """
    print("I can't work like this...")
    if message is None:
        print("Please start again with a path for me to work with...")
    else:
        print(message)


def error(msg):
    error_message = msg if len(msg) > 0 else "An error has occurred"
    print(error_message)
    print(__doc__)
    exit()


if __name__ == '__main__':
    import sys

    main(sys.argv)
